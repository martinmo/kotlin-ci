package demo

import demo.ci.pipeline
import demo.ci.run
import demo.ci.seconds
import demo.ci.steps.docker.docker
import demo.ci.steps.docker.mounts
import demo.ci.steps.echo
import demo.ci.steps.gitInfo
import demo.ci.steps.gitStatus
import demo.ci.steps.gitUpdate
import demo.ci.steps.sh
import demo.ci.timeout
import java.time.Instant

fun main(args: Array<String>) {

    pipeline(dataDir = "data") {

        //gitUpdate("https://bitbucket.org/martinmo/kotlin-ci/src/master/")
        gitUpdate("C:\\Work\\ci\\data\\demo")

        val mounts = mounts().addVolumeCache(volume = "gradle", path = "/home/gradle/.gradle")

        docker("gradle:4.8.1-jdk8", mounts) {
            sh("gradle", "build")
            sh("ls", "-lah")
        }

        timeout(1.seconds()) {
            // sleep(2.seconds())
        }

        gitStatus()
        sh("git", "status")

        step("use the context") {
            echo("build number ${ctx.buildNumber}, git ref ${ctx.gitInfo().ref}")
        }

        step("Can nest") {
            val gitStatusText: String = step("deep") {
                echo("step has a result")
                sh("git", "status")
            }

            assert(gitStatusText.contains("master"))
        }


        step("flickering test") {
            if (Instant.now().epochSecond % 3 == 0L) {
                throw RuntimeException("fails sometimes")
            }
        }

    }.run().trigger()
}
