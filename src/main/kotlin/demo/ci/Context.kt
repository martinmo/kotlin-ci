package demo.ci

import org.slf4j.LoggerFactory
import java.io.File

data class Context(
        val dirs: Dirs,
        val buildNumber: Int,
        val names: List<String> = emptyList(),
        val data: MutableMap<String, Any> = mutableMapOf()
) {
    val log = LoggerFactory.getLogger(name())

    val workspace = dirs.workspace

    var counter = 0

    fun name(): String = names.joinToString(">")

    fun subContext(name: String): Context = copy(names = names + ("[${++counter}]$name"))
}

const val RESULT_FILENAME = "result.json"

class Dirs(dir: String) {

    private val log = LoggerFactory.getLogger("DIR")

    val baseDir: File = File(dir)
    val workspace: File = File(baseDir, "workspace")
    val caches: File = File(baseDir, "caches")
    val archive: File = File(baseDir, "archive")

    init {
        baseDir.mkdirs()
        workspace.mkdirs()
        caches.mkdirs()
        archive.mkdirs()
    }

    fun archiveForBuild(buildNumber: Int, createIfMissing: Boolean = true): File {
        val dir = File(archive, buildNumber.toString())
        if (createIfMissing && !dir.exists()) {
            dir.mkdirs()
        }
        return dir
    }

    fun readHistory(): List<BuildResult> {
        return archive.listFiles()
                .filter { it.isDirectory }
                .filter { it.name != "unknown" }
                .map {
                    try {
                        readJson<BuildResult>(File(it, RESULT_FILENAME))
                    } catch (e: Exception) {
                        log.error("failed to read state from {}", e)
                        null
                    }
                }
                .filterNotNull()
                .map {
                    if (it.state != ExecutionState.SUCCESS && it.state != ExecutionState.FAILURE) {
                        it.copy(state = ExecutionState.UNKNOWN)
                    } else {
                        it
                    }
                }
                .sortedBy { it.buildNumber }
    }

    fun saveResult(buildResult: BuildResult) {
        val dir = archiveForBuild(buildResult.buildNumber)
        dir.mkdirs()

        val file = File(dir, RESULT_FILENAME)
        buildResult.saveAsJson(file)
    }
}