package demo.ci.steps

import demo.ci.Context
import demo.ci.PipelineRunner
import demo.ci.readJson
import demo.ci.saveAsJson
import org.eclipse.jgit.api.Git
import org.eclipse.jgit.api.ResetCommand
import org.eclipse.jgit.lib.Constants
import org.eclipse.jgit.lib.ObjectId
import org.slf4j.Logger
import java.io.File

fun PipelineRunner.gitUpdate(repo: String, branch: String = "master") {
    step("GIT") {
        pullOrClone(ctx.workspace, repo, branch, ctx.log).use { git ->

            val head = git.log().setMaxCount(1).call().first()

            val commits: List<Commit> = git.commitsSince(ctx.findPreviousCommit())

            val gitInfo = GitInfo(
                    repo = repo,
                    ref = head.name,
                    message = head.shortMessage,
                    refShort = head.abbreviate(7).name()!!,
                    branch = git.repository.branch,
                    commits = commits.drop(1)
            )
            ctx.data["git"] = gitInfo

            gitInfo.saveAsJson(File(ctx.dirs.archiveForBuild(ctx.buildNumber), "git.json"))

            ctx.log.info(gitInfo.toString())
        }
    }
}

private fun pullOrClone(workspace: File, repo: String, branch: String, log: Logger): Git {
    return if (File(workspace, ".git").exists()) {
        log.info("pulling {}", repo)
        val git = Git.open(workspace)
        git.reset().setMode(ResetCommand.ResetType.HARD).call()
        git.checkout().setName(branch).call()
        git.pull().call()
        git
    } else {
        log.info("cloning {}", repo)

        Git.cloneRepository()
                .setURI(repo)
                .setBranch(branch)
                .setDirectory(workspace)
                .call()
    }
}

fun Context.gitInfo(): GitInfo = this.data["git"] as GitInfo

data class GitInfo(
        val repo: String,
        val ref: String,
        val branch: String,
        val refShort: String,
        val message: String,
        val commits: List<Commit>
)

fun PipelineRunner.gitStatus() {
    step("GIT") {
        Git.open(ctx.workspace).use {
            val status = it.status().call()
            ctx.log.info("Clean ${status.isClean}")
            ctx.log.info("Changes ${status.uncommittedChanges}")
        }
    }
}

fun Context.findPreviousCommit(): String? {
    val dir = this.dirs.archiveForBuild(this.buildNumber - 1, false)
    if (!dir.exists()) {
        return null
    }
    try {
        val gitInfo: GitInfo = readJson(File(dir, "git.json"))
        return gitInfo.ref
    } catch (e: Exception) {
        log.error("failed to load git info", e)
        return null
    }
}

fun Git.commitsSince(commt: String?): List<Commit> {
    val commits = if (commt == null) {
        this.log().setMaxCount(100).call()
    } else {
        this.log().addRange(ObjectId.fromString(commt), repository.resolve(Constants.HEAD)).call()
    }

    return commits.map { Commit(it.name!!, it.shortMessage) }
}

data class Commit(val rev: String, val message: String) {
    fun shortRev() = rev.slice(0..6)
}