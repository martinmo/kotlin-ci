package demo.ci.steps

import demo.ci.PipelineRunner
import org.zeroturnaround.exec.ProcessExecutor
import org.zeroturnaround.exec.stream.LogOutputStream

fun PipelineRunner.sh(vararg cmd: String): String {
    val isWindows = System.getProperty("os.name")
            .toLowerCase().startsWith("windows")

    return if (isWindows) {
        executeCommand(listOf("cmd.exe", "/c") + cmd.toList())
    } else {
        executeCommand(listOf("/bin/sh", "-c") + cmd.joinToString(" "))
    }
}

fun PipelineRunner.executeCommand(cmd: List<String>): String {
    return step("EXEC") {
        ctx.log.info("executing (path: {}) {}", ctx.workspace, cmd)
        val output = StringBuilder()
        val result = ProcessExecutor()
                .directory(ctx.workspace)
                .command(cmd)
                .redirectOutput(object : LogOutputStream() {
                    override fun processLine(line: String) {
                        ctx.log.info("> $line")
                        output.appendln(line)
                    }
                })
                .execute()
        if (result.exitValue != 0) {
            throw RuntimeException("Execution failed. $cmd returned ${result.exitValue}")
        }
        return@step output.toString()
    }
}
