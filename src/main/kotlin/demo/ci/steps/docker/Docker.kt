package demo.ci.steps.docker

import com.spotify.docker.client.DefaultDockerClient
import com.spotify.docker.client.DockerClient
import com.spotify.docker.client.messages.ContainerConfig
import com.spotify.docker.client.messages.HostConfig
import demo.ci.Context
import demo.ci.PipelineRunner
import org.slf4j.Logger

private val docker = DefaultDockerClient.fromEnv().build()

fun PipelineRunner.docker(image: String, mounts: Mounts? = null, b: DockerBuilder.() -> Unit) {
    step("DOCKER") {
        withRunningContainer(image, false, mounts
                ?: Mounts(ctx.dirs), ctx.log) { _, containerId ->
            b(DockerBuilder(containerId, ctx))
        }
    }
}

class DockerBuilder(val containerId: String, val ctx: Context) {

    fun sh(vararg it: String) {
        ctx.log.info("executing {}", it)

        val execCreation = docker.execCreate(containerId, it,
                DockerClient.ExecCreateParam.attachStdout(),
                DockerClient.ExecCreateParam.attachStderr()
        )

        val logStream = docker.execStart(execCreation.id())

        logStream.attach(LogBridge(ctx).start(false),
                LogBridge(ctx).start(true))

        // block
        logStream.readFully()

        val execResult = docker.execInspect(execCreation.id())
        if (execResult.exitCode() != 0) {
            throw RuntimeException("docker exec failed with exit code " + execResult.exitCode())
        }
    }
}

fun withRunningContainer(
        image: String,
        allaysPull: Boolean,
        mounts: Mounts,
        log: Logger,
        f: (docker: DockerClient, containerId: String) -> Unit) {

    pullImage(image, allaysPull, log)

    val hostConfig = HostConfig.builder()
            .appendBinds(*mounts.list.toTypedArray())
            .build()

    val containerConfig = ContainerConfig.builder()
            .hostConfig(hostConfig)
            .image(image)
            .cmd("sh", "-c", "while :; do sleep 1; done")
            .workingDir("/workspace")
            .build()

    val creation = docker.createContainer(containerConfig)
    val id = creation.id()!!

    try {
        docker.startContainer(id)
        f(docker, id)
    } finally {
        try {
            docker.killContainer(id)
        } catch (e: Exception) {
            log.error("failed to kill container")
        }
        docker.removeContainer(id)
    }
}

private fun pullImage(image: String, allaysPull: Boolean, log: Logger) {
    val found = docker.listImages(DockerClient.ListImagesParam.byName(image))
    if (allaysPull || found.isEmpty()) {
        log.info("pulling image", image)
        docker.pull(image)
    } else {
        log.debug("not pulling image", image)
    }
}
