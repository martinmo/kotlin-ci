package demo.ci.steps.docker

import com.spotify.docker.client.messages.HostConfig
import com.spotify.docker.client.messages.Volume
import demo.ci.Dirs
import demo.ci.PipelineRunner
import java.io.File

data class Mounts(val dirs: Dirs, val list: List<HostConfig.Bind> = listOf(workSpaceMount(dirs))) {

    fun addFileCache(name: String, path: String, readOnly: Boolean = false): Mounts {
        val file = File(dirs.caches, name)
        file.mkdirs()

        val bind = HostConfig.Bind.from(file.absolutePath)
                .to(path)
                .readOnly(readOnly)
                .build()
        return copy(list = list + bind)
    }

    fun addVolumeCache(volume: String, path: String, readOnly: Boolean = false): Mounts {
        val bind = HostConfig.Bind.from(Volume.builder().name(volume).build())
                .to(path)
                .readOnly(readOnly)
                .build()
        return copy(list = list + bind)
    }

    companion object {
        fun workSpaceMount(dirs: Dirs) = HostConfig.Bind.from(dirs.workspace.absolutePath)
                .to("/workspace")
                .readOnly(false)
                .build()
    }
}

fun PipelineRunner.mounts(): Mounts = Mounts(ctx.dirs)