package demo.ci.steps.docker

import demo.ci.Context
import demo.ci.applyMdc
import java.io.BufferedReader
import java.io.InputStreamReader
import java.io.OutputStream
import java.io.PipedInputStream
import java.io.PipedOutputStream

class LogBridge(val ctx: Context) {
    private val inStream = PipedInputStream()
    val outStream = PipedOutputStream(inStream)

    fun start(isErrorStream: Boolean): OutputStream {
        Thread {
            ctx.applyMdc()
            ctx.log.debug("log bridge started")
            try {

                val reader = BufferedReader(InputStreamReader(inStream))
                var line: String? = reader.readLine()
                while (line != null) {
                    if (isErrorStream) {
                        ctx.log.error("!> $line")
                    } else {
                        ctx.log.info("> $line")
                    }

                    line = reader.readLine()
                }
            } catch (e: Exception) {
                ctx.log.error("failed with", e)
            } finally {
                inStream.close()
                outStream.close()
            }
            ctx.log.debug("log bridge finished")
        }.start()
        return outStream
    }
}