package demo.ci.steps

import demo.ci.PipelineRunner
import demo.ci.format
import org.slf4j.LoggerFactory
import java.time.Duration

private val echo_log = LoggerFactory.getLogger("ECHO")

fun PipelineRunner.echo(msg: String) {
    step("ECHO") {
        ctx.log.info(msg)
    }
}

private val sleep_log = LoggerFactory.getLogger("SLEEP")

fun PipelineRunner.sleep(duration: Duration) {
    step("SLEEP") {
        ctx.log.info("Sleeping " + duration.format())
        Thread.sleep(duration.toMillis())
    }
}
