package demo.ci

import org.slf4j.MDC
import java.time.Duration
import java.time.Instant

data class Pipeline(
        val runner: PipelineRunner.() -> Unit,
        val dirs: Dirs
) {
    fun execute(ctx: Context): BuildResult {
        ctx.applyMdc()

        val started = Instant.now()

        ctx.log.info(HASH_LINE)
        ctx.log.info("#  Starting Pipeline {}", ctx.buildNumber)
        ctx.log.info(HASH_LINE)

        val result = timed {
            val builder = PipelineRunner(ctx)
            runner(builder)
        }

        val stateAndMessage = when (result) {
            is TimerResult.Failure -> Pair(ExecutionState.FAILURE, result.error.message ?: "")
            is TimerResult.Success -> Pair(ExecutionState.SUCCESS, ExecutionState.SUCCESS.name)
        }

        val buildResult = BuildResult(started, result.duration, ctx.buildNumber, stateAndMessage.first, stateAndMessage.second)
        ctx.log.info("Finished {}", buildResult)
        ctx.log.info("")
        return buildResult
    }
}

fun pipeline(dataDir: String, runner: PipelineRunner.() -> Unit): Pipeline {
    val dirs = Dirs(dataDir)
    return Pipeline(runner, dirs)
}

class PipelineRunner(val ctx: Context) {
    fun <T> step(name: String, runner: PipelineRunner.() -> T): T {
        ctx.log.info("# Next Step: $name")

        val result = timed {
            val subContext = ctx.subContext(name)
            subContext.applyMdc()
            val inner = PipelineRunner(subContext)
            runner(inner)
        }

        try {
            when (result) {
                is TimerResult.Failure -> {
                    ctx.log.error("Step failed with {}", result.error.message, result.error)
                    throw result.error
                }
                is TimerResult.Success -> {
                    return result.value
                }
            }

        } finally {
            ctx.log.info("Step took " + result.duration.format())
            ctx.log.info(MINUS_LINE)
            ctx.applyMdc()
        }
    }
}

fun PipelineRunner.timeout(maxDuration: Duration, inner: PipelineRunner.() -> Unit) {
    step("TIMEOUT") {
        val start = Instant.now()
        inner(this)
        val duration = Duration.between(start, Instant.now())
        if (duration > maxDuration) {
            throw RuntimeException("timeout $duration")
        }
    }
}

fun Context.applyMdc() {
    MDC.put("BUILD_NUMBER", buildNumber.toString())
    MDC.put("BUILD_CONTEXT", name())
}

val HASH_LINE = (1..60).map { "#" }.joinToString("")
val MINUS_LINE = (1..60).map { "-" }.joinToString("")
