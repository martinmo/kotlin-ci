package demo.ci

import org.slf4j.LoggerFactory
import java.util.Collections
import java.util.concurrent.LinkedBlockingQueue

class PipelineExecutor(val pipeline: Pipeline) : AutoCloseable {
    private val log = LoggerFactory.getLogger(PipelineExecutor::class.java)
    val state = State(pipeline)

    fun trigger(): Int {
        return state.trigger()
    }

    fun queueLength(): Int = state.queueLength()

    val thread = Thread {
        while (!Thread.interrupted()) {
            try {
                val next = state.pending.take()
                state.update(next.running())
                val ctx = Context(pipeline.dirs, next.buildNumber, listOf("MAIN"))
                val result = pipeline.execute(ctx)
                state.update(result)
            } catch (e: Exception) {
                log.error("execution failed", e)
            }
        }
    }

    init {
        thread.name = "EXECUTOR"
        thread.start()
    }

    override fun close() {
        thread.interrupt()
    }
}

fun Pipeline.run(): PipelineExecutor {
    val executor = PipelineExecutor(this)
    Server(executor).start()
    return executor
}


class State(val pipeline: Pipeline) {
    private val log = LoggerFactory.getLogger(State::class.java)
    private val results: MutableMap<Int, BuildResult> = Collections.synchronizedMap(pipeline.dirs.readHistory().groupBy { it.buildNumber }.mapValues { it.value.first() })
    val pending = LinkedBlockingQueue<BuildResult>()

    var currentBuildNumber = results.map { it.value.buildNumber }.max() ?: 0

    fun trigger(): Int {
        currentBuildNumber++
        val executionState = BuildResult.pending(currentBuildNumber)
        pending.add(executionState)
        results[currentBuildNumber] = executionState
        return currentBuildNumber
    }

    fun queueLength(): Int = pending.size

    fun update(result: BuildResult) {
        results[result.buildNumber] = result
        try {
            pipeline.dirs.saveResult(result)
        } catch (e: Exception) {
            log.error("failed to save", e)
        }
    }

    fun executions(): List<BuildResult> = results.values.sortedByDescending { it.buildNumber }
}
