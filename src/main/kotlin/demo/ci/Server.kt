package demo.ci

import demo.ci.steps.GitInfo
import org.apache.commons.io.input.Tailer
import org.apache.commons.io.input.TailerListenerAdapter
import org.http4k.core.Method
import org.http4k.core.Response
import org.http4k.core.Status.Companion.OK
import org.http4k.core.Status.Companion.TEMPORARY_REDIRECT
import org.http4k.core.noCache
import org.http4k.lens.Path
import org.http4k.lens.int
import org.http4k.routing.bind
import org.http4k.routing.routes
import org.http4k.routing.websockets
import org.http4k.server.Http4kServer
import org.http4k.server.Jetty
import org.http4k.server.asServer
import org.http4k.websocket.PolyHandler
import org.http4k.websocket.Websocket
import org.http4k.websocket.WsMessage
import org.slf4j.LoggerFactory
import java.io.File
import java.lang.Exception

class Server(private val pipelineExecutor: PipelineExecutor) : AutoCloseable {

    val log = LoggerFactory.getLogger("SERVER")
    var http4k: Http4kServer? = null

    private val dirs = pipelineExecutor.pipeline.dirs
    private val state = pipelineExecutor.state

    fun start() {
        val buildNumberLens = Path.int().of("buildNumber")

        val app = routes(
                "/trigger" bind Method.GET to {
                    val buildNumber = pipelineExecutor.trigger()
                    Response(TEMPORARY_REDIRECT).header("Location", "/tail/$buildNumber")
                },
                "/" bind Method.GET to {
                    val builds = state.executions()
                    val gitInfo = loadGitInfo(builds, dirs)
                    Response(OK).noCache().body(list(builds, pipelineExecutor.queueLength(), gitInfo))
                },
                "/logs/{buildNumber:.*}" bind Method.GET to {
                    val file = File(dirs.archiveForBuild(buildNumberLens(it)), "build.log")
                    Response(OK).body(file.inputStream())
                },
                "/tail/{buildNumber:.*}" bind Method.GET to {
                    Response(OK).body(tail(pipelineExecutor.queueLength(), buildNumberLens(it)))
                }
        )
        val ws = websockets(
                "/ws/{buildNumber:.*}" bind { ws: Websocket ->
                    val buildNumber = buildNumberLens(ws.upgradeRequest)
                    val file = File(dirs.archiveForBuild(buildNumber), "build.log")
                    val tail = LogTail(file) {
                        ws.send(WsMessage(it))
                    }.start()

                    ws.onClose { tail.close() }
                }
        )

        http4k = PolyHandler(app, ws).asServer(Jetty(8080)).start()
        log.info("Server running: http://localhost:8080/")
    }

    override fun close() {
        http4k?.stop()
    }
}

fun loadGitInfo(builds: List<BuildResult>, dirs: Dirs): Map<Int, GitInfo> {
    val log = LoggerFactory.getLogger("loadGitInfo")
    return builds.mapNotNull {
        try {
            val gitInfo:GitInfo = readJson(File(dirs.archiveForBuild(it.buildNumber), "git.json"))
            it.buildNumber to gitInfo
        }catch (e:Exception){
            log.error("failed to load git info", e)
            null
        }
    }.groupBy { it.first }.mapValues { it.value.first().second }
}

class LogTail(val file: File, val handler: (String) -> Unit) : AutoCloseable {

    private val log = LoggerFactory.getLogger(LogTail::class.java)

    private val tailer = Tailer(file, object : TailerListenerAdapter() {
        override fun handle(ex: Exception) {
            log.error("failed", ex)
        }

        override fun fileNotFound() {
            log.error("file not found", file.absolutePath)
        }

        override fun handle(line: String) {
            handler(line)
        }

    })

    fun start(): LogTail {
        Thread(tailer).start()
        return this
    }

    override fun close() {
        tailer.stop()
    }
}
