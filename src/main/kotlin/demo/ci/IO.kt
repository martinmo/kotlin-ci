package demo.ci

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.readValue
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import java.io.File

val mapper = ObjectMapper().registerKotlinModule().registerModule(JavaTimeModule())

fun <T> T.saveAsJson(file: File): Unit = file.outputStream().use {
    return mapper.writeValue(it, this)
}

inline fun <reified T : kotlin.Any> readJson(file: File): T = file.inputStream().use {
    return mapper.readValue(it)
}