package demo.ci

import java.time.Duration
import java.time.Instant
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter


fun <T> timed(f: () -> T): TimerResult<T> {
    val start = Instant.now()
    try {
        val result = f()
        val duration = Duration.between(start, Instant.now())
        return TimerResult.Success(duration, result)
    } catch (e: RuntimeException) {
        val duration = Duration.between(start, Instant.now())
        return TimerResult.Failure(duration, e)
    }
}

sealed class TimerResult<T>(val duration: Duration) {
    class Success<T>(duration: Duration, val value: T) : TimerResult<T>(duration)
    class Failure<T>(duration: Duration, val error: Exception) : TimerResult<T>(duration)
}

fun Duration.format(): String {
    val seconds = this.seconds
    val absSeconds = Math.abs(seconds)
    val positive = String.format(
            "%d:%02d:%02d.%03d",
            absSeconds / 3600,
            absSeconds % 3600 / 60,
            this.seconds,
            this.toMillis() % 1000)
    return if (seconds < 0) "-$positive" else positive
}

fun ZonedDateTime.format(): String {
    return DateTimeFormatter.ISO_LOCAL_DATE_TIME.format(this)
}

fun Instant.humanize(): String = this.atZone(java.time.ZoneId.systemDefault()).format()

fun Int.seconds(): Duration = Duration.ofSeconds(this.toLong())