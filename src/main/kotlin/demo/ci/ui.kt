package demo.ci

import demo.ci.steps.GitInfo
import kotlinx.html.MAIN
import kotlinx.html.ScriptType
import kotlinx.html.a
import kotlinx.html.body
import kotlinx.html.div
import kotlinx.html.footer
import kotlinx.html.h1
import kotlinx.html.head
import kotlinx.html.html
import kotlinx.html.id
import kotlinx.html.link
import kotlinx.html.main
import kotlinx.html.meta
import kotlinx.html.p
import kotlinx.html.pre
import kotlinx.html.script
import kotlinx.html.span
import kotlinx.html.stream.appendHTML
import kotlinx.html.style
import kotlinx.html.table
import kotlinx.html.tbody
import kotlinx.html.td
import kotlinx.html.th
import kotlinx.html.thead
import kotlinx.html.title
import kotlinx.html.tr
import kotlinx.html.unsafe
import java.io.StringWriter

fun list(results: List<BuildResult>, queue: Int, gitInfos: Map<Int, GitInfo>): String {
    return layout(queue) {
        table(classes = "table") {
            thead {
                tr {
                    th { +"No" }
                    th { +"State" }
                    th { +"Start" }
                    th { +"Duration" }
                    th { +"Logs" }
                    th { +"Git" }
                    th { +"Message" }
                }
            }
            tbody {
                results.sortedByDescending { it.buildNumber }.forEach {
                    tr {
                        td { +it.buildNumber.toString() }
                        td { +it.state.name }
                        td { +it.startTime.humanize() }
                        td { +(it.duration?.format() ?: "") }
                        td {
                            a(href = "/logs/${it.buildNumber}") { +"Logs" }
                            if (it.state == ExecutionState.RUNNING) {
                                a(href = "/tail/${it.buildNumber}") { +"Tail" }
                            }
                        }
                        td {
                            val gitInfo = gitInfos[it.buildNumber]
                            +(gitInfo?.let { it.refShort + " " + it.message } ?: "")
                            div {
                                style = "font-size:smaller;"
                                gitInfo?.commits?.forEach {
                                    p { +(it.shortRev() + " " + it.message) }
                                }
                            }
                        }
                        td { +it.message }
                    }
                }
            }
        }
    }
}

fun tail(queue: Int, buildNumber: Int): String {
    return layout(queue) {

        pre() {
            id = "logs"
        }

        script(type = ScriptType.textJavaScript) {
            unsafe {
                +"""
                    var logs = document.getElementById("logs");
                    var connection = new WebSocket('ws://localhost:8080/ws/$buildNumber');
                    connection.onmessage = function (e) {
                      logs.innerText += (e.data + "\n")
                    };
                """.trimIndent()
            }
        }
    }
}

fun layout(queue: Int, block: MAIN.() -> Unit): String = StringWriter().appendHTML().html {
    head {
        title {
            +"CI"
        }
        meta(name = "viewport", content = "width=device-width, initial-scale=1.0")
        link(href = "https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css", rel = "stylesheet")
        script(src = "https://code.jquery.com/jquery-3.3.1.slim.min.js") {}
        script(src = "https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js") {}
    }
    body {


        main(classes = "container") {
            a(href = "/") {
                h1 {
                    +"CI"
                }
            }

            div {
                span {
                    +"Queue: $queue "
                }
                a(href = "/trigger") {
                    +"Trigger"
                }
            }

            block()
        }

        footer {

        }
    }
}.toString()
