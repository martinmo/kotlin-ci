package demo.ci

import demo.ci.ExecutionState.PENDING
import demo.ci.ExecutionState.RUNNING
import java.time.Duration
import java.time.Instant

enum class ExecutionState {
    PENDING, RUNNING, SUCCESS, FAILURE, UNKNOWN
}

data class BuildResult(
        val startTime: Instant,
        val duration: Duration? = null,
        val buildNumber: Int,
        val state: ExecutionState,
        val message: String = state.name) {

    fun running(): BuildResult = this.copy(state = RUNNING)

    companion object {
        fun pending(buildNumber: Int): BuildResult = BuildResult(
                startTime = Instant.now(),
                buildNumber = buildNumber,
                state = PENDING
        )
    }
}

