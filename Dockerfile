FROM openjdk:8u171-jdk

ADD build/distributions/ci.tar /

WORKDIR /ci

CMD ["/ci/bin/ci"]
