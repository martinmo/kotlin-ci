# Kotlin CI prototype
Define CI pipeline with Kotlin

```kotlin
fun main(args: Array<String>) {

    pipeline(dataDir = "data") {

        gitUpdate("https://bitbucket.org/martinmo/kotlin-ci/src/master/")

        val mounts = mounts().addVolumeCache(volume = "gradle", path = "/home/gradle/.gradle")

        docker("gradle:4.8.1-jdk8", mounts) {
            sh("gradle", "build")
            sh("ls", "-lah")
        }

        timeout(1.seconds()) {
            // sleep(2.seconds())
        }

        gitStatus()
        sh("git", "status")

        step("use the context") {
            echo("build number ${ctx.buildNumber}, git ref ${ctx.gitInfo().ref}")
        }

        step("Can nest") {
            val gitStatusText: String = step("deep") {
                echo("step has a result")
                sh("git", "status")
            }

            assert(gitStatusText.contains("master"))
        }


        step("flickering test") {
            if (Instant.now().epochSecond % 3 == 0L) {
                throw RuntimeException("fails sometimes")
            }
        }

    }.run().trigger()
}


```
